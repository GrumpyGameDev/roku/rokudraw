Function CreateResources()
    resources = {}
    resources.fontRegistry = CreateObject("roFontRegistry")
    resources.fonts = {}
    resources.fonts.default = resources.fontRegistry.GetDefaultFont()
    return resources
End Function