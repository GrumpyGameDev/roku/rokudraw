Function CreatePlay() As Object
    Return {
        fontname:"default",
        backgroundcolor:&h000000FF,
        foregroundcolor:&hebebebff,
        cursorcolor:15,
        x:0,
        y:0,
        palette:[&h000000ff,&h0000AAFF,&h00AA00FF,&h00aaaaFF,&haa0000ff,&haa00aaff,&haa5500ff,&haaaaaaff,&h555555ff,&h5555ffff,&h55ff55ff,&h55ffffff,&hff5555ff,&hff55ffff,&hffff55ff,&hffffffff],
        pendown:false,

        start: Function(context) As Void
        End Function,

        finish: Function (context) As Void
        End Function,

        draw: Function(context,screen) As Void
            screen.clear(m.backgroundcolor)
            for column=0 to context.data.columns-1
                for row=0 to context.data.rows-1
                    x = context.data.cellwidth * column
                    y = context.data.cellheight * row
                    if (m.x=column and m.y=row)
                        screen.DrawRect(x,y,context.data.cellwidth,context.data.cellheight,m.palette[m.cursorcolor])
                        screen.DrawRect(x+4,y+4,context.data.cellwidth-8,context.data.cellheight-8,m.palette[context.data.grid[column][row]])
                    else
                        screen.DrawRect(x,y,context.data.cellwidth,context.data.cellheight,m.palette[context.data.grid[column][row]])
                    end if
                end for
            end for
        End Function,

        handle: Function (context, event) As String
            if(type(event)="roUniversalControlEvent")
                button = event.GetInt()
                if(button=0)'back
                    return "mainmenu"
                else if(button=2)
                    m.y=(m.y + context.data.rows-1) MOD context.data.rows
                    if(m.pendown)
                        context.data.grid[m.x][m.y]=m.cursorcolor
                    end if
                else if(button=3)
                    m.y=(m.y + 1) MOD context.data.rows
                    if(m.pendown)
                        context.data.grid[m.x][m.y]=m.cursorcolor
                    end if
                else if(button=4)
                    m.x=(m.x + context.data.columns-1) MOD context.data.columns
                    if(m.pendown)
                        context.data.grid[m.x][m.y]=m.cursorcolor
                    end if
                else if(button=5)
                    m.x=(m.x + 1) MOD context.data.columns
                    if(m.pendown)
                        context.data.grid[m.x][m.y]=m.cursorcolor
                    end if
                else if(button=6)
                    context.data.grid[m.x][m.y]=m.cursorcolor
                else if(button=8)
                    m.cursorcolor = (m.cursorcolor + m.palette.Count() - 1) MOD m.palette.Count()
                    print m.cursorcolor
                    if(m.pendown)
                        context.data.grid[m.x][m.y]=m.cursorcolor
                    end if
                else if(button=9)
                    m.cursorcolor = (m.cursorcolor + 1) MOD m.palette.Count()
                    if(m.pendown)
                        context.data.grid[m.x][m.y]=m.cursorcolor
                    end if
                else if(button=13)
                    m.pendown=not m.pendown
                    if(m.pendown)
                        context.data.grid[m.x][m.y]=m.cursorcolor
                    end if
                end if
            end if
            Return "play"
        End Function
    }
End Function