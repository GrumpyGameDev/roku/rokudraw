Function CreateConfirmQuit() As Object
    Return {
        items:["No", "Yes"],
        fontname:"default",
        backgroundcolor:&h000000FF,
        inactivecolor:&hebebebff,
        activecolor:&hebeb00ff,
        promptColor:&h7575ebff,
        currentIndex:0,

        start: Function(context) As Void
        End Function,

        finish: Function (context) As Void
        End Function,

        draw: Function(context,screen) As Void
            lineHeight = context.resources.fonts[m.fontname].GetOneLineHeight() 
            screen.clear(m.backgroundcolor)
            y=0
            screen.DrawText("Are you sure?", 0, y, m.promptColor, context.resources.fonts[m.fontname])
            y = y + lineHeight
            index=0
            for each item in m.items
                if (index=m.currentIndex)
                    screen.DrawText(item, 0, y, m.activecolor, context.resources.fonts[m.fontname])
                else
                    screen.DrawText(item, 0, y, m.inactivecolor, context.resources.fonts[m.fontname])
                end if
                index=index+1
                y = y + lineHeight
            end for
        End Function,

        handle: Function (context, event) As String
            if(type(event)="roUniversalControlEvent")
                button = event.GetInt()
                if(button=2)'up
                    m.currentIndex=(m.currentIndex+m.items.Count()-1) MOD m.items.Count()
                else if(button=3)'down
                    m.currentIndex=(m.currentIndex+1) MOD m.items.Count()
                else if(button=6)'select
                    if(m.currentIndex=1)
                        return ""
                    else
                        return "mainmenu"
                    end if
                else if(button=0)'back
                    return "mainmenu"
                end if
            end if
            Return "confirmquit"
        End Function
    }
End Function