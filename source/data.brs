Function CreateData()
    data= {
        columns:32,
        rows:18,
        cellwidth:40,
        cellheight:40,
    }
    data.grid = []
    for column=0 to data.columns-1
        data.grid.push([])
        for row=0 to data.rows-1
            data.grid[column].push(0)
        end for
    end for
    return data
End Function